import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class Main extends Component {
  render() {
    return (
      <div className="h-screen relative bottom-[136px] left-[300px] flex bg-[#d1efef] ">
        <h1 className="text-2xl ml-5 mt-2">🚀 Welcome to Homepage!</h1>
        <div className="relative top-[100px] right-[200px] rounded-lg  text-center ">
          <NavLink
            className="bg-white h-20 w-20 px-16 py-8  underline rounded-lg  drop-shadow-2xl text-2xl "
            to={"/Articles"}
          >
            Articles Page
          </NavLink>
        </div>
      </div>
    );
  }
}

export default Main;
