import React, { Component } from "react";
import { Link } from "react-router-dom";
import Data from "./Data";
import { useParams } from "react-router-dom";

function ArticlesPages() {
  const intial = useParams();
  const { slug } = intial;
  return (
    <div className="h-screen w-full relative bottom-[136px] left-[300px]  bg-[#d1efef] ">
      <Link
        className="underline text-purple-600 ml-6 font-bold mt-10"
        to={"/Articles"}
      >
        👈 Go Back To Articles
      </Link>
      <h3 className="text-xl mt-6 ml-6">
        The slug of the article is:::
        <span className="ml-2 text-xl font-bold">{slug}</span>
      </h3>
    </div>
  );
}
export default ArticlesPages;
