import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import Data from "./Data";
class Articles extends Component {
  constructor() {
    super();
    this.state = {
      ChangeData: Data,
    };
  }
  inputEvent = (event) => {
    let initialData = Data.filter((ele) => {
      return ele.title.toLowerCase().includes(event.target.value);
    });
    this.setState({ ChangeData: initialData });
  };
  render() {
    return (
      <>
        <div className="h-screen w-full relative bottom-[136px] left-[300px]  bg-[#d1efef] ">
          <input
            onChange={this.inputEvent}
            className="ml-10 mt-5 h-12 w-[400px] pl-6"
            placeholder="Search"
          />
          {this.state.ChangeData.map((ele, index) => {
            return (
              <>
                <div className="ml-14 leading-8 mt-6" key={index}>
                  <NavLink to={`/ArticlesPages/${ele.slug}`}>
                    {" "}
                    <h3
                      className="underline underline-offset-1 text-xl"
                      key={ele.title}
                    >
                      {ele.title}
                    </h3>
                  </NavLink>
                  <p className="" key={ele.author}>
                    {ele.author}
                  </p>
                </div>
              </>
            );
          })}
        </div>
      </>
    );
  }
}

export default Articles;
