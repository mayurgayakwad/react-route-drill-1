import React, { Component } from "react";
import { Outlet } from "react-router-dom";
import Dashboard from "./Dashboard";
import Sidebar from "./Sidebar";

class Home extends Component {
  render() {
    return (
      <>
        <Dashboard />
        <Sidebar />
        <Outlet />
      </>
    );
  }
}

export default Home;
