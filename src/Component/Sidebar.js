import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./sidebarStyle.css";

class Sidebar extends Component {
  render() {
    return (
      <div className=" flex flex-col w-[300px]">
        <NavLink className="text-xl px-10 py-5 mr-2 hover:bg-gray-200" to={"/"}>
          Home
        </NavLink>
        <NavLink
          className="text-xl px-10 py-5 mr-2 hover:bg-gray-200"
          to={"/Articles"}
        >
          Articles
        </NavLink>
      </div>
    );
  }
}

export default Sidebar;
