import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import React, { Component } from "react";
import Main from "./Component/Main";
import Articles from "./Component/Articles";
import Home from "./Component/Home";
import ArticlesPages from "./Component/ArticlesPages";
import Error from "./Component/Error";

class App extends Component {
  render() {
    return (
      <>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home />}>
              <Route path="/" element={<Main />} />
              <Route path="/Articles" element={<Articles />} />
              <Route path="/ArticlesPages/:slug" element={<ArticlesPages />} />
            </Route>
            <Route path="*" element={<Error />} />
          </Routes>
        </BrowserRouter>
      </>
    );
  }
}

export default App;
